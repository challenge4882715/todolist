const mongoose = require("mongoose");
const userSchema = require("./userModel")
// Define comment schema
const commentSchema = new mongoose.Schema({
    commentText: {
        type: String,
        required: [true, "Please provide comment text!"]
    },
    Creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    taskId: {
        type: String,
        required: [true, "Please provide task id !"]
    },

    createdAt: {
        type: Date,
        default: Date.now
    }
});

// Define Comment model
const Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;
