const mongoose = require("mongoose");
const commentSchema = require("./commentModel");
const userSchema = require("./userModel")
const groupModel = require("./groupModel")

const progressEnum = ['inprogress', 'completed', 'pending'];
const priorityEnum = ['low', 'medium', 'high'];

// Define task schema
const taskSchema = new mongoose.Schema({
    taskTitle: {
        type: String,
        required: [true, "Please provide task title!"]
    },
    taskDescription: {
        type: String,
        required: [true, "Please provide task description!"]
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    dueDate: {
        type: Date,
        required: [true, "Please provide task due date!"]
    },
    Creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    taskGroup: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Group",
        default: null
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment' // Reference to the Comment model
    }],
    Recurring: {
        type: String,
        required: [true, "Task recurring is required"]
    },
    dependent: {
        type: String,
        default:"none "
    },

    progress: {
        type: String,
        enum: progressEnum,
        default: 'pending' // Default value if not provided
    },

    priority: {
        type: String,
        enum: priorityEnum,
        default: "low"
    }
});

// Define Task model
const Task = mongoose.model('Task', taskSchema);

module.exports = Task;
