const mongoose = require("mongoose");
const User = require("./userModel");
const taskSchema = require("./commentModel")

const groupSchema = new mongoose.Schema({
    groupTitle: {
        type: String,
        required: [true, "Group name is required !"]
    },
    groupDescription: {
        type: String,
        required: [true, "Group description is required"]
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    member: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }],
    tasks: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Task"
    }],
    groupCreator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    }
})

const Group = mongoose.model("Group", groupSchema);

module.exports = Group;