const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config({ path: "config.env" });
const app = require("./app.js");

const DB = process.env.DATABASE.replace(
    "PASSWORD",
    process.env.DATABASE_PASSWORD
);

mongoose.connect(DB).then((con) => {
    console.log(`Database connected to ${con.connection.host}`);
}).catch(error => console.log(error));

const httpServer = require("http").createServer(app);
const io = require("socket.io")(httpServer);

const port = 6969;

httpServer.listen(port, () => {
    console.log(`App running on port http://localhost:${port} ..`);
});

io.on("connection", (socket) => {
    console.log("A user connected", socket.id);

    socket.on("disconnect", () => {
        console.log("A user disconnected");
    });

    socket.on('userUpdated', (data) => {
        console.log("User updated - Emitting userUpdated event:", data);
    });

    socket.on('userDeleted', (data) => {
        console.log("User deleted - Emitting userDeleted event:", data);
    });
});
