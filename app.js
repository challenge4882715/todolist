const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const path = require("path");
const app = express();

app.use(cors());
app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.json());
app.use(express.static(path.join(__dirname, "views")));

const httpServer = require("http").createServer(app);
const io = require("socket.io")(httpServer);

const userRoute = require("./routes/userRoutes")(io);
const commentRoute = require("./routes/commentRouter");
const TaskRoute = require("./routes/taskRouter");
const GroupRoute = require("./routes/groupRoutes");
const viewRoute = require("./routes/viewRouter");
const notificationRoute = require("./routes/notificationRoutes")

app.use("/justdoit/user", userRoute);
app.use("/justdoit/comment", commentRoute);
app.use("/justdoit/task", TaskRoute);
app.use("/justdoit/group", GroupRoute);
app.use("/justdoit/notification", notificationRoute);
app.use("/", viewRoute);


module.exports = app;
