
const express = require('express')
const router = express.Router()
const viewsController = require('./../controller/viewController')

const userRoute = require("./../controller/userController")

router.get('/', viewsController.getIndexPage)
router.get('/task',  viewsController.getTaskPage)
router.get('/group',  viewsController.getGroupPage)
router.get('/login', viewsController.getLoginPage)
router.get('/signup', viewsController.getRegistrationPage)



module.exports = router;