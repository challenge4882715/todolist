const express = require('express');
const userController = require("../controller/userController");

function userRouter(io) {
    const router = express.Router();

    // Middleware to emit events for user updates and deletions
    router.use((req, res, next) => {
        // Emit event when user is updated
        res.on('finish', () => {
            if (req.method === 'PATCH' && res.statusCode === 200) {
                // console.log("User updated - Emitting userUpdated event:", { id: req.params.id, data: req.body });
                io.emit('userUpdated', { id: req.params.id, data: req.body });
            }
        });

        // Emit event when user is deleted
        res.on('finish', () => {
            if (req.method === 'DELETE' && res.statusCode === 200) {
                // console.log("User deleted - Emitting userDeleted event:", { id: req.params.id });
                io.emit('userDeleted', { id: req.params.id });
            }
        });

        next();
    });

    router.post("/login", userController.login);
    router.post("/logout", userController.logout);

    router
        .route('/')
        .get(userController.getAllUser)
        .post(userController.createUser);

    router.route("/:id")
        .get(userController.getUser)
        .patch(userController.updateUser)
        .delete(userController.deleteUser);

    return router;
}

module.exports = userRouter;
