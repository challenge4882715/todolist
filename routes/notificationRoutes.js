const express = require("express")

const notificationController = require("../controller/notificationController")
const router = express.Router();

router.route("/")
    .post(notificationController.createNotification)
    .get(notificationController.getAllNotification)

router.route("/:id")
    .get(notificationController.getNotificationsForUser)
    .delete(notificationController.deleteNotification)


module.exports = router