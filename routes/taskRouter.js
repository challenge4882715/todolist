const Taskcontroller = require("../controller/taskController");
const express = require("express");
const router = express.Router();

router.route("/")
    .get(Taskcontroller.getAllTasks)
    .post(Taskcontroller.createTask);

router.route("/:id")
    .get(Taskcontroller.getTask)
    .patch(Taskcontroller.updateTask)
    .delete(Taskcontroller.deleteTask);
    
module.exports = router;
