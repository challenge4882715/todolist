const GroupController = require("../controller/groupController")
const express = require("express");
const router = express.Router();

router.route("/")
    .get(GroupController.getAllGroups)
    .post(GroupController.createGroup);

router.route("/:id")
    .get(GroupController.getGroup)
    .patch(GroupController.updateGroup)
    .delete(GroupController.deleteGroup);

module.exports = router;