
const taskbutton = document.getElementById("create-task");
taskbutton.addEventListener("click", () => {
    const taskform = document.querySelector(".createTask-modal-container")
    taskform.style.opacity = "1";
    taskform.style.zIndex = "1";

    // Close the comment when clicking outside the comment modal after a short delay
    setTimeout(() => {
        document.addEventListener("click", CloseTaskModalOutside);
    }, 100);
})



function CloseTaskModalOutside(event) {
    const commentModal = document.querySelector(".createTask-modal-container");
    if (!commentModal.contains(event.target)) {
        // If the clicked element is not inside the comment modal, close the modal
        commentModal.style.opacity = "0";
        commentModal.style.zIndex = "-1";
        document.removeEventListener("click", CloseTaskModalOutside); // Remove the event listener
    }
}