async function fetchGroups() {
    try {
        const response = await axios.get('http://localhost:6969/justdoit/group');
        const groups = Object.values(response.data.data);
        const groupContainer = document.querySelector(".all-group-container");

        for (const group of groups) {
            const usersResponse = await axios.get('http://localhost:6969/justdoit/user');
            const userArray = usersResponse.data.data;
            if (group.groupCreator._id === obj._id) {

                const groupMemberContainer = document.querySelector(".Group-member-container")

                const groupMember = group.member;
                groupMember.forEach((member) => {
                    groupMemberContainer.innerHTML += `
                    <div class="group-member">
                            <img src="./images/image.webp" alt="Member 1">
                            <h4>  ${member.name}</h4>
                        </div>
                    `
                })


                let userOptions = "";
                userArray.forEach((user) => {
                    if (!group.member.some(member => member._id === user._id)) { // Check if user ID is not included in group members
                        userOptions += `<option value="${user._id}" checked>${user.name}</option>`;
                    }
                });

                groupContainer.innerHTML += `
                    <div class="task-card">
                        <div class="task-title-container">
                            <div class="circle"></div>
                            <div>${group.groupTitle}<span class="priority High">High</span></div>
                        </div>
                        <div class="task-description-container">${group.groupDescription}</div>
                        <div class="task-footer-container">
                            <div class="group-container">
                                <span class="material-symbols-outlined">groups</span>
                                <div>${group.member.length}</div>
                            </div>
                            <form id = "addMemberManual" > 
                    
                            <div class="form-item-adduser">
                                <select name="member" class="priority-container">
                                    <option value="none" checked>None</option>
                                    ${userOptions}
                                </select>
                                <button type="submit" id="${group._id}" class="addMember">Add Member</button>
                                </div>
                            </form>
                           
                        </div>
                    </div>`;
            }
        }


        attachCommentIconListeners();
    } catch (error) {
        console.error('Error fetching groups:', error);
    }
}

fetchGroups();
// setInterval(fetchGroups, 5000);

var obj

if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
} else {
    obj = JSON.parse('{}')
}


function attachCommentIconListeners() {

    document.getElementById("profile-pic-icon").addEventListener("click", () => {
        // Show the profile drop-down
        const profileDropDown = document.querySelector(".profile-drop-down");
        profileDropDown.style.opacity = "1";
        profileDropDown.style.zIndex = "1";

        // Close the profile drop-down when clicking outside after a short delay
        setTimeout(() => {
            document.addEventListener("click", closeProfilePopupOutside);
        }, 100);
    });


    document.querySelectorAll("#addMemberManual").forEach((form) => {
        form.addEventListener("submit", async (e) => {
            e.preventDefault();
            const formData = new FormData(form);
            const GroupData = Object.fromEntries(formData.entries());
            console.log(GroupData.member)
            // Get the button ID
            const groupid = form.querySelector(".addMember").getAttribute("id");

            try {
                const response = await axios.patch(`http://localhost:6969/justdoit/group/${groupid}`, GroupData);
                console.log(response)
                groupForm.reset();
            } catch (error) {
                console.error('Error group task :', error.message);
            }
        });
    });


}

document.querySelectorAll("#addMemberManual").forEach((form) => {
    console.log(form)
})

const groupForm = document.getElementById("createGroupForm");

async function GetUser() {
    const response = await axios.get('http://localhost:6969/justdoit/user');
    const users = response.data.data;

    users.forEach((user) => {
        document.getElementById("userContainer").innerHTML += `
        <option value="${user._id}" checked>${user.name}</option>
                    `
    })
}

GetUser();

groupForm.addEventListener('submit', async (e) => {
    e.preventDefault();


    const formData = new FormData(groupForm);
    const GroupData = Object.fromEntries(formData.entries());
    console.log(GroupData);

    GroupData.groupCreator = obj._id; // Adding the _id property to GroupData



    try {
        const response = await axios.post('http://localhost:6969/justdoit/group', GroupData);
        console.log(response)
        groupForm.reset();

        const notificationData = {
            "sender": obj._id,
            "recipient": GroupData.member,
            "message": `You have been added to  a ${GroupData.groupTitle} by ${obj.name}`,
            "type": "invitation"
        }

        const createNotification = await axios.post("http://localhost:6969/justdoit/notification", notificationData);
        console.log(createNotification)


    } catch (error) {
        console.error('Error group task :', error.message);
    }

});


// Function to close the profile drop-down when clicking outside
function closeProfilePopupOutside(event) {
    const profileDropDown = document.querySelector(".profile-drop-down");
    if (!profileDropDown.contains(event.target)) {
        profileDropDown.style.opacity = "0";
        profileDropDown.style.zIndex = "-1";
        document.removeEventListener("click", closeProfilePopupOutside); // Remove the event listener
    }
}

document.getElementById("setting-profile").addEventListener("click", () => {
    const profileSettingModal = document.querySelector(".profile-modal-container");
    profileSettingModal.style.opacity = "1"
    profileSettingModal.style.zIndex = "1"
})


document.querySelector(".setting-close").addEventListener("click", () => {
    const profileSettingModal = document.querySelector(".profile-modal-container");
    profileSettingModal.style.opacity = "0"
    profileSettingModal.style.zIndex = "-1"
})

