document.getElementById("profile-pic-icon").addEventListener("click", () => {
    // Show the profile drop-down
    const profileDropDown = document.querySelector(".profile-drop-down");
    profileDropDown.style.opacity = "1";
    profileDropDown.style.zIndex = "1";

    // Close the profile drop-down when clicking outside after a short delay
    setTimeout(() => {
        document.addEventListener("click", closeProfilePopupOutside);
    }, 100);
});

// Function to close the profile drop-down when clicking outside
function closeProfilePopupOutside(event) {
    const profileDropDown = document.querySelector(".profile-drop-down");
    if (!profileDropDown.contains(event.target)) {
        profileDropDown.style.opacity = "0";
        profileDropDown.style.zIndex = "-1";
        document.removeEventListener("click", closeProfilePopupOutside); // Remove the event listener
    }
}

document.getElementById("setting-profile").addEventListener("click", () => {
    const profileSettingModal = document.querySelector(".profile-modal-container");
    profileSettingModal.style.opacity = "1"
    profileSettingModal.style.zIndex = "1"
})


document.querySelector(".setting-close").addEventListener("click", () =>{
    const profileSettingModal = document.querySelector(".profile-modal-container");
    profileSettingModal.style.opacity = "0"
    profileSettingModal.style.zIndex = "-1"
})
