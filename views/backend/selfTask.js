

const TaskForm = document.getElementById('CreateIndividualTask');

async function fetchTasks() {
    try {
        const response = await axios.get('http://localhost:6969/justdoit/task');

        const tasksArray = response.data.data;
        console.log(tasksArray)
        tasksArray.forEach((task) => {

            const date = new Date(task.dueDate);

            const options = {

                weekday: 'long',

                month: 'long',

                day: 'numeric',

                timeZone: 'UTC'

            };

            const formatter = new Intl.DateTimeFormat('en-US', options);

            const formattedDate = formatter.format(date);


            const dependentcontainer = document.getElementById("dependentContainer")
            if (task.Creator._id === obj._id && task.taskGroup === null) {

                dependentcontainer.innerHTML += `
                <option value="${task._id}">${task.taskTitle}</option>
                
                `
            }

            if (task.Creator._id === obj._id && task.taskGroup === null) {

                if (task.progress === "inprogress") {

                    const taskContaainer = document.querySelector(".mytask-container")
                    taskContaainer.innerHTML += `
                <div class="task-card">
                    <div class="task-title-container">
                        <div class="circle">
                        </div>
                        <div>
                            ${task.taskTitle}
                            &nbsp;
                            &nbsp;
                            <span class="due-date">
                                ${formattedDate}
                            </span>
                            &nbsp;
                            &nbsp;
                            <span class="priority High">
                                ${task.priority}
                            </span>
                        </div>
                    </div>
    
                    <div class="task-description-container">
                        ${task.taskDescription}
                    </div>
    
                    <div class="task-footer-container">
                        <div class="group-container">
                            <span class="material-symbols-outlined">
                                group_work
                            </span>
                            <div>
                            </div>
                        </div>
                        <form id = "addMemberManual" > 
                        <div class="form-item-adduser">
                        <select name="progress" class="priority-container">
                            <option value="completed" checked>Completed</option>
                            <option value="inprogress" checked>Inprogress</option>
                            
                        </select>
                        <button type="submit" id="${task._id}" class="addMember">Update Progress</button>
                        </div>
                        </form>
                                    </div>
                </div>
                 `
                }
                if (task.progress === "pending") {

                    const taskContaainer = document.querySelector(".myPendingtask-container")
                    taskContaainer.innerHTML += `
                <div class="task-card">
                    <div class="task-title-container">
                        <div class="circle">
                        </div>
                        <div>
                            ${task.taskTitle}
                            &nbsp;
                            &nbsp;
                            <span class="due-date">
                                ${formattedDate}
                            </span>
                            &nbsp;
                            &nbsp;
                            <span class="priority High">
                                ${task.priority}
                            </span>
                        </div>
                    </div>
    
                    <div class="task-description-container">
                        ${task.taskDescription}
                    </div>
    
                    <div class="task-footer-container">
                        <div class="group-container">
                            <span class="material-symbols-outlined">
                                group_work
                            </span>
                            <div>
                            </div>
                        </div>
                        <form id = "addMemberManual" > 
                        <div class="form-item-adduser">
                        <select name="progress" class="priority-container">
                            <option value="completed" checked>Completed</option>
                            <option value="inprogress" checked>Inprogress</option>
                            
                        </select>
                        <button type="submit" id="${task._id}" class="addMember">Update Progress</button>
                        </div>
                        </form>
                                    </div>
                </div>
                                                 `
                }
                if (task.progress === "completed") {

                    const taskContaainer = document.querySelector(".myCompletedtask-container")
                    taskContaainer.innerHTML += `
                <div class="task-card">
                    <div class="task-title-container">
                        <div class="circle">
                        </div>
                        <div>
                            ${task.taskTitle}
                            &nbsp;
                            &nbsp;
                            <span class="due-date">
                                ${formattedDate}
                            </span>
                            &nbsp;
                            &nbsp;
                            <span class="priority High">
                                ${task.priority}
                            </span>
                        </div>
                    </div>
    
                    <div class="task-description-container">
                        ${task.taskDescription}
                    </div>
    
                    <div class="task-footer-container">
                    <div class="group-container">
                        <span class="material-symbols-outlined">
                            group_work
                        </span>
                        <div>
                        </div>
                    </div>
                    <form id = "addMemberManual" > 
                    <div class="form-item-adduser">
                    <select name="progress" class="priority-container">
                        <option value="completed" checked>Completed</option>
                        <option value="inprogress" checked>Inprogress</option>
                        
                    </select>
                    <button type="submit" id="${task._id}" class="addMember">Update Progress</button>
                    </div>
                    </form>
                                </div>
                </div>
                                                 `
                }
            }


        })


        attachCommentIconListeners();

    } catch (error) {
        console.error('Error fetching tasks:', error);
    }
}

fetchTasks();

// setInterval(fetchTasks, 5000);

var obj


if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
    document.getElementById("task-creator").value = obj._id;

} else {
    obj = JSON.parse('{}')
}

TaskForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const formData = new FormData(TaskForm);
    const taskData = Object.fromEntries(formData.entries());
    taskData.Creator = obj._id
    console.log(taskData)
    try {
        const response = await axios.post('http://localhost:6969/justdoit/task', taskData);
        TaskForm.reset();

    } catch (error) {
        console.error('Error creating task :', error.response.data.message);
    }
});




async function attachCommentIconListeners() {
    document.querySelectorAll(".comment-icon").forEach(commenticon => {
        commenticon.addEventListener("click", async () => {
            let Taskid = commenticon.getAttribute("id");

            const commentModal = document.querySelector(".comment-modal-container")
            commentModal.style.opacity = "1";
            commentModal.style.zIndex = "1";


            // Close the comment when clicking outside the comment modal after a short delay
            setTimeout(() => {
                document.addEventListener("click", closeCommentModalOutside);
            }, 100);
        })

    });



    document.querySelectorAll("#addMemberManual").forEach((form) => {
        form.addEventListener("submit", async (e) => {
            e.preventDefault();
            const formData = new FormData(form);
            const taskData = Object.fromEntries(formData.entries());
            console.log(taskData)
            // Get the button ID
            const taskid = form.querySelector(".addMember").getAttribute("id");
            try {
                const response = await axios.patch(`http://localhost:6969/justdoit/task/${taskid}`, taskData);
                form.reset();
            } catch (error) {
                console.error('Error updating task :', error.message);
            }
        });
    });
}