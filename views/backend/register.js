const registrationForm = document.getElementById('registrationForm');

registrationForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const formData = new FormData(registrationForm);
    const userData = Object.fromEntries(formData.entries());

    console.log(userData)
    try {
        const response = await axios.post('http://localhost:6969/justdoit/user/', userData);
        console.log(response.data);
        alert('Registration successful!');
        registrationForm.reset();
        location.assign("/login")
    } catch (error) {
        console.error('Error registering user:', error.response.data);
        alert('Registration failed. Please try again.');
    }
});