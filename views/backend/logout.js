

const logout = async () => {
    try {
        const res = await axios({
            method: 'POST',
            url: "http://localhost:6969/justdoit/user/logout"
        })

        console.log(res.data)
        if (res.data.status === "success") {
            // Delete the token cookie
            document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

            location.assign("/login");
        }

    } catch (err) {
        console.log(err.message)
    }
}


document.querySelector(".logout-user-btn").addEventListener("click", () => {
    logout();
})  