const TaskForm = document.getElementById('taskForm');

async function fetchTasks() {
    try {
        const response = await axios.get('http://localhost:6969/justdoit/task');

        const tasksArray = response.data.data;
        console.log(tasksArray)
        tasksArray.forEach((task) => {

            if (task.taskGroup != null && task.taskGroup.member !== null && Array.isArray(task.taskGroup.member) && task.taskGroup.member.includes(obj._id)) {
                console.log('user inclided in group')
                const date = new Date(task.dueDate);

                const options = {

                    weekday: 'long',

                    month: 'long',

                    day: 'numeric',

                    timeZone: 'UTC'

                };

                const formatter = new Intl.DateTimeFormat('en-US', options);

                const formattedDate = formatter.format(date);


                const dependentcontainer = document.getElementById("dependentContainer")

                if (task.taskGroup !== null) {
                    dependentcontainer.innerHTML += `
                     <option value="${task._id}">${task.taskTitle}</option>
        
                    `
                }

                if (task.Creator._id === obj._id && task.taskGroup !== null) {

                    if (task.progress === "inprogress") {

                        const taskContaainer = document.querySelector(".mytask-container")
                        taskContaainer.innerHTML += `
                    <div class="task-card">
                        <div class="task-title-container">
                            <div class="circle update-user-progress">
                            </div>
                            <div>
                                ${task.taskTitle}
                                &nbsp;
                                &nbsp;
                                <span class="due-date">
                                    ${formattedDate}
                                </span>
                                &nbsp;
                                &nbsp;
                                <span class="priority High">
                                    ${task.priority}
                                </span>
                            </div>
                        </div>
        
                        <div class="task-description-container">
                            ${task.taskDescription}
                        </div>
        
                        <div class="task-footer-container">
                            <div class="group-container">
                                <span class="material-symbols-outlined">
                                    group_work
                                </span>
                                <div>
                                    ${task.taskGroup.groupTitle}
                                </div>
                            </div>
        
                            <div class="group-container">
                        
                                <div class="card-profile-container">
                                    <div>
                                        <img src="./images/image.webp" class="user-profile-image" alt="">
                                    </div>
                                    <div>
                                    ${task.Creator.name}
                                    </div>
                                </div>
                                <form id="addMemberManual">
                                <div class="form-item-adduser update-task">
                                    <select name="progress" class="priority-container update-progress">
                                        <option value="inprogress" selected>Inprogress</option>
                                        <option value="pending">Pending</option>
                                        <option value="completed">Completed</option>
                                    </select>
                                    <button type="submit" id="${task._id}" class="addMember update-progress">Update
                                        Progress</button>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                                                     `
                    }
                    if (task.progress === "pending") {

                        const taskContaainer = document.querySelector(".myPendingtask-container")
                        taskContaainer.innerHTML += `
                    <div class="task-card">
                        <div class="task-title-container">
                            <div class="circle update-user-progress">
                            </div>
                            <div>
                                ${task.taskTitle}
                                &nbsp;
                                &nbsp;
                                <span class="due-date">
                                    ${formattedDate}
                                </span>
                                &nbsp;
                                &nbsp;
                                <span class="priority High">
                                    ${task.priority}
                                </span>
                            </div>
                        </div>
        
                        <div class="task-description-container">
                            ${task.taskDescription}
                        </div>
        
                        <div class="task-footer-container">
                        <div class="group-container">
                        <span class="material-symbols-outlined">
                            group_work
                        </span>
                        <div>
                            ${task.taskGroup.groupTitle}
                        </div>
                    </div>
        
                            <div class="group-container">
                            
                                <div class="card-profile-container">
                                    <div>
                                        <img src="./images/image.webp" class="user-profile-image" alt="">
                                    </div>
                                    <div>
                                    ${task.Creator.name}
                                    </div>
                                </div>
                                <form id="addMemberManual">
                                <div class="form-item-adduser update-task">
                                    <select name="progress" class="priority-container update-progress">
                                        <option value="inprogress" selected>Inprogress</option>
                                        <option value="pending">Pending</option>
                                        <option value="completed">Completed</option>
                                    </select>
                                    <button type="submit" id="${task._id}" class="addMember update-progress">Update
                                        Progress</button>
                                </div>
                            </form>
                            </div>
                            
                        </div>
                    </div>
                                                     `
                    }
                    if (task.progress === "completed") {

                        const taskContaainer = document.querySelector(".myCompletedtask-container")
                        taskContaainer.innerHTML += `
                    <div class="task-card">
                        <div class="task-title-container">
                            <div class="circle update-user-progress">
                            </div>
                            <div>
                                ${task.taskTitle}
                                &nbsp;
                                &nbsp;
                                <span class="due-date">
                                    ${formattedDate}
                                </span>
                                &nbsp;
                                &nbsp;
                                <span class="priority High">
                                    ${task.priority}
                                </span>
                            </div>
                        </div>
        
                        <div class="task-description-container">
                            ${task.taskDescription}
                        </div>
        
                        <div class="task-footer-container">
                        <div class="group-container">
                        <span class="material-symbols-outlined">
                            group_work
                        </span>
                        <div>
                            ${task.taskGroup.groupTitle}
                        </div>
                    </div>
        
                            <div class="group-container">
                           
                            <div class="card-profile-container">
                                <div>
                                    <img src="./images/image.webp" class="user-profile-image" alt="">
                                </div>
                                <div>
                                ${task.Creator.name}
                                </div>
                            </div>
                            <form id="addMemberManual">
                            <div class="form-item-adduser update-task">
                                <select name="progress" class="priority-container update-progress">
                                    <option value="inprogress" selected>Inprogress</option>
                                    <option value="pending">Pending</option>
                                    <option value="completed">Completed</option>
                                </select>
                                <button type="submit" id="${task._id}" class="addMember update-progress">Update
                                    Progress</button>
                            </div>
                        </form>
                        </div>
                        </div>
                    </div>
                                                     `
                    }


                }

                if (task.Creator._id !== obj._id && task.taskGroup !== null) {

                    if (task.progress === "pending") {
                        const pendingTaskContainer = document.querySelector(".task-containerPending");

                        pendingTaskContainer.innerHTML += `
                    <div class="task-card">
                        <div class="task-title-container">
                            <div class="circle update-user-progress">
                            </div>
                            <div>
                                ${task.taskTitle}
                                &nbsp;
                                &nbsp;
                                <span class="due-date">
                                    ${formattedDate}
                                </span>
                                &nbsp;
                                &nbsp;
                                <span class="priority High">
                                    ${task.priority}
                                </span>
                            </div>
                        </div>
        
                        <div class="task-description-container">
                            ${task.taskDescription}
                        </div>
        
                        <div class="task-footer-container">
                        <div class="group-container">
                        <span class="material-symbols-outlined">
                            group_work
                        </span>
                        <div>
                            ${task.taskGroup.groupTitle}
                        </div>
                    </div>
        
                            <div class="group-container">
                            <span class="material-symbols-outlined comment-icon" id = "${task._id}">
                                comment
                            </span>
                            <div class="card-profile-container">
                                <div>
                                    <img src="./images/image.webp" class="user-profile-image" alt="">
                                </div>
                                <div>
                                ${task.Creator.name}
                                </div>
                            </div>
                            <form id="addMemberManual">
                            <div class="form-item-adduser update-task">
                                <select name="progress" class="priority-container update-progress">
                                    <option value="inprogress" selected>Inprogress</option>
                                    <option value="pending">Pending</option>
                                    <option value="completed">Completed</option>
                                </select>
                                <button type="submit" id="${task._id}" class="addMember update-progress">Update
                                    Progress</button>
                            </div>
                        </form>
                        </div>
                        </div>
                    </div>
                    `
                    }
                    if (task.progress === "inprogress") {
                        const progressTaskContainer = document.querySelector(".task-container");
                        progressTaskContainer.innerHTML += `
                    <div class="task-card">
                        <div class="task-title-container">
                            <div class="circle update-user-progress">
                            </div>
                            <div>
                                ${task.taskTitle}
                                &nbsp;
                                &nbsp;
                                <span class="due-date">
                                    ${formattedDate}
                                </span>
                                &nbsp;
                                &nbsp;
                                <span class="priority High">
                                    ${task.priority}
                                </span>
                            </div>
                        </div>
        
                        <div class="task-description-container">
                            ${task.taskDescription}
                        </div>
        
                        <div class="task-footer-container">
                        <div class="group-container">
                        <span class="material-symbols-outlined">
                            group_work
                        </span>
                        <div>
                            ${task.taskGroup.groupTitle}
                        </div>
                    </div>
        
                            <div class="group-container">
                            <span class="material-symbols-outlined comment-icon" id = "${task._id}">
                                comment
                            </span>
                            <div class="card-profile-container">
                                <div>
                                    <img src="./images/image.webp" class="user-profile-image" alt="">
                                </div>
                                <div>
                                ${task.Creator.name}
                                </div>
                            </div>
    
                            <form id="addMemberManual">
                            <div class="form-item-adduser update-task">
                                <select name="progress" class="priority-container update-progress">
                                    <option value="inprogress" selected>Inprogress</option>
                                    <option value="pending">Pending</option>
                                    <option value="completed">Completed</option>
                                </select>
                                <button type="submit" id="${task._id}" class="addMember update-progress">Update
                                    Progress</button>
                            </div>
                        </form>
                        </div>
                        </div>
                    </div>
                    `
                    }
                    if (task.progress === "completed") {
                        const progressTaskContainer = document.querySelector(".task-containerCompleted");
                        progressTaskContainer.innerHTML += `
                    <div class="task-card">
                        <div class="task-title-container">
                            <div class="circle update-user-progress">
                            </div>
                            <div>
                                ${task.taskTitle}
                                &nbsp;
                                &nbsp;
                                <span class="due-date">
                                    ${formattedDate}
                                </span>
                                &nbsp;
                                &nbsp;
                                <span class="priority High">
                                    ${task.priority}
                                </span>
                            </div>
                        </div>
        
                        <div class="task-description-container">
                            ${task.taskDescription}
                        </div>
        
                        <div class="task-footer-container">
                        <div class="group-container">
                        <span class="material-symbols-outlined">
                            group_work
                        </span>
                        <div>
                            ${task.taskGroup.groupTitle}
                        </div>
                    </div>
        
                            <div class="group-container">
                                <span class="material-symbols-outlined comment-icon" id = "${task._id}">
                                    comment
                                </span>
                                <div class="card-profile-container">
                                    <div>
                                        <img src="./images/image.webp" class="user-profile-image" alt="">
                                    </div>
                                    <div>
                                    ${task.Creator.name}
                                    </div>
                                </div>
                            </div>
                            <form id="addMemberManual">
                            <div class="form-item-adduser update-task">
                                <select name="progress" class="priority-container update-progress">
                                    <option value="inprogress" selected>Inprogress</option>
                                    <option value="pending">Pending</option>
                                    <option value="completed">Completed</option>
                                </select>
                                <button type="submit" id="${task._id}" class="addMember update-progress">Update
                                    Progress</button>
                            </div>
                        </form>
                        </div>
                    </div>
                    `
                    }
                }
            }


        })


        attachCommentIconListeners();

    } catch (error) {
        console.error('Error fetching tasks:', error);
    }
}

fetchTasks();
// setInterval(fetchTasks, 5000);

var obj


if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
    document.getElementById("task-creator").value = obj._id;
    document.querySelector(".comment-profile-name").innerHTML = obj.name;

} else {
    obj = JSON.parse('{}')
}

TaskForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const formData = new FormData(TaskForm);
    const taskData = Object.fromEntries(formData.entries());
    console.log(taskData)
    try {
        const response = await axios.post('http://localhost:6969/justdoit/task', taskData);
        TaskForm.reset();




    } catch (error) {
        console.error('Error creating task :', error.response.data.message);
    }
});

var commentCount = 0

async function attachCommentIconListeners() {
    document.querySelectorAll(".comment-icon").forEach(commenticon => {
        commenticon.addEventListener("click", async () => {
            let Taskid = commenticon.getAttribute("id");
            console.log(Taskid)
            CreateComment(Taskid)
            fetchComment(Taskid)
            const commentModal = document.querySelector(".comment-modal-container")
            commentModal.style.opacity = "1";
            commentModal.style.zIndex = "1";


            // Close the comment when clicking outside the comment modal after a short delay
            setTimeout(() => {
                document.addEventListener("click", closeCommentModalOutside);
            }, 100);
        })

    });


    document.querySelectorAll(".update-user-progress").forEach(progress => {
        progress.addEventListener("click", async (event) => {

            // Find the closest form element to the clicked progress element
            const progressForm = progress.closest(".task-card").querySelector("#addMemberManual");

            // If a form is found, make it visible
            if (progressForm) {
                progressForm.style.opacity = "1";
                progressForm.style.zIndex = "1";
            }

            // Close the comment when clicking outside the comment modal after a short delay
            setTimeout(() => {
                document.addEventListener("click", closeProgressModalOutsideOnce);
            }, 100);
        });
    });

    document.querySelectorAll("#addMemberManual").forEach((form) => {
        form.addEventListener("submit", async (e) => {
            e.preventDefault();
            const formData = new FormData(form);
            const taskData = Object.fromEntries(formData.entries());

            const taskid = form.querySelector(".addMember").getAttribute("id");
            console.log(taskid)

            try {
                const response = await axios.patch(`http://localhost:6969/justdoit/task/${taskid}`, taskData);
                console.log(response.data.data)
                const task = response.data.data

                if (task) {

                    const getGroup = await axios.get("http://localhost:6969/justdoit/group/" + task.taskGroup);
                    const member = getGroup.data.data.member

                    if (member) {
                        const notificationData = {
                            "sender": obj._id,
                            "recipient": member,
                            "message": `${obj.name} added task to ${task.progress} Task name : ${task.taskTitle}`,
                            "type": "invitation"
                        }
                        console.log(notificationData)
                        const createNotification = await axios.post("http://localhost:6969/justdoit/notification", notificationData);
                        console.log(createNotification)
                    }
                }

                form.reset();


            } catch (error) {
                console.error('Error updating task :', error.message);
            }
        });
    });


}

function closeProgressModalOutsideOnce(event) {
    const progressForms = document.querySelectorAll("#addMemberManual");
    progressForms.forEach(form => {
        // Check if the clicked element is not inside the form
        if (!form.contains(event.target)) {
            // Hide the form
            form.style.opacity = "0";
            form.style.zIndex = "-1";
            // Remove the event listener once the form is closed
            document.removeEventListener("click", closeProgressModalOutsideOnce);
        }
    });
}

// async function setCommentCount() {
//     const response = await axios.get(`http://localhost:6969/justdoit/task/${TASKID}`);
//     const task = response.data.data;

//     const comments = task.comments;
//     console.log(comments)
//     document.querySelectorAll(".comment-icon").forEach((commenticon) => {
//         commentCount = comments.length;
//         commenticon.setAttribute('data-comment-count', commentCount);
//     });
// }
// setCommentCount();

async function CreateComment(Taskid) {

    const CommentForm = document.querySelector("#commentForm");
    CommentForm.addEventListener("submit", async (e) => {
        e.preventDefault();
        const formData = new FormData(CommentForm);
        const taskData = Object.fromEntries(formData.entries());
        taskData.taskId = Taskid;

        try {
            const response = await axios.post(`http://localhost:6969/justdoit/comment/${obj._id}`, taskData);
            console.log('Comment created:', response.data);
            CommentForm.reset()
            // Handle success
        } catch (error) {
            console.error('Error creating comment:', error);
            // Handle error
        }
    });
}


async function fetchComment(Taskid) {
    try {
        const response = await axios.get(`http://localhost:6969/justdoit/task/${Taskid}`);
        const task = response.data.data;

        const comments = task.comments;
        const commentContainer = document.querySelector(".comment-container")

        if (comments.length !== 0) {


            comments.forEach(async (comment) => {

                const date = new Date(comment.createdAt);

                const options = {

                    weekday: 'long',

                    month: 'long',

                    day: 'numeric',

                    timeZone: 'UTC'

                };

                const formatter = new Intl.DateTimeFormat('en-US', options);

                const formattedDate = formatter.format(date);

                const getUser = await axios.get(`http://localhost:6969/justdoit/user/${comment.Creator}`)
                commentContainer.innerHTML += `
            <div>
            <div class="comment-profile-container">
                <div>
                    <img src="./images/image.webp" class="user-profile-image" alt="">
                </div>
                <div>
                    ${getUser.data.data.name}
                </div>
                <div class="comment-dot">
                </div>
                <div class="comment-date">
                    ${formattedDate}
                </div>
    
            </div>
    
            <div class="comment">
                ${comment.commentText}
            </div>
        </div>
            `
            })
        }
        // Handle success
    } catch (error) {
        console.error('Error getting comment:', error);
        // Handle error
    }
}




function closeCommentModalOutside(event) {
    const commentModal = document.querySelector(".comment-modal-container");
    const commentContainer = document.querySelector(".comment-container")
    if (!commentModal.contains(event.target)) {
        // If the clicked element is not inside the comment modal, close the modal
        commentModal.style.opacity = "0";
        commentModal.style.zIndex = "-1";
        document.removeEventListener("click", closeCommentModalOutside); // Remove the event listener
        commentContainer.innerHTML = ``;
    }
}


async function fetchGroup() {
    try {
        const response = await axios.get('http://localhost:6969/justdoit/group');
        const groups = Object.values(response.data.data);
        const groupContainer = document.getElementById("groupContainer");

        groups.forEach((group) => {
            console.log(group.groupTitle)
            groupContainer.innerHTML += `
            <option value="${group._id}">${group.groupTitle}</option>
            `
        })



        attachCommentIconListeners();
    } catch (error) {
        console.error('Error fetching groups:', error);
    }
}

fetchGroup();