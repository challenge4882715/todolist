const notificationModal = document.querySelector('.user-notification-modal');


var obj

if (document.cookie) {
    obj = JSON.parse(document.cookie.substring(6))
} else {
    obj = JSON.parse('{}')
}

document.querySelector(".user-notification").addEventListener("click", () => {
    notificationModal.style.zIndex = "1";
    notificationModal.style.opacity = "1";

    // Close the modal when clicking outside the modal after a short delay
    setTimeout(() => {
        document.addEventListener("click", closeNotificationModalOutside);
    }, 100);

    getUserNotification();
})

function closeNotificationModalOutside(event) {
    const notificationModal = document.querySelector('.user-notification-modal');

    // Check if the clicked element is not inside the notification modal
    if (!notificationModal.contains(event.target)) {
        // Hide the notification modal
        notificationModal.style.opacity = "0";
        notificationModal.style.zIndex = "-1";
        // Remove the event listener once the modal is closed
        notificationModal.innerHTML = ""
        document.removeEventListener("click", closeNotificationModalOutside);
    }
}


async function getUserNotification() {
    const notification = await axios.get("http://localhost:6969/justdoit/notification/" + obj._id)
    const notifications = notification.data.data
    console.log(notifications.length)


    showNotification(notifications)
}

async function showNotificationPopup() {
    try {
        const notification = await axios.get("http://localhost:6969/justdoit/notification/" + obj._id);
        const notifications = notification.data.data;
        const notificationCount = notifications.length;

        // Get the user-notification element
        const userNotificationElement = document.querySelector('.user-notification');

        // Set the data-notification-count attribute
        userNotificationElement.setAttribute('data-notification-count', notificationCount);
    } catch (error) {
        console.error('Error fetching notifications:', error);
    }
}


showNotificationPopup();
setInterval(showNotificationPopup, 5000);


function showNotification(notifications) {
    notifications.forEach(notification => {
        notificationModal.innerHTML += `
        <div class="notification-user">
            ${notification.message}
        </div>
        `
    });
}