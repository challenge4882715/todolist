// Replace 'http://localhost:6969' with the URL of your server
const socket = io('http://localhost:6969');
console.log(socket)

// Listen for the 'userUpdated' event
socket.on('userUpdated', (data) => {
    console.log("User updated - Emitting userUpdated event:", data);
});

socket.on('userDeleted', (data) => {
    console.log("User deleted - Emitting userDeleted event:", data);
});



// Function to update user information
const updateUser = async (userData, userId) => {
    try {
        console.log(userData);
        const response = await axios({
            method: 'PATCH',
            url: `http://localhost:6969/justdoit/user/${userId}`,
            data: userData, // Pass the data object here
            headers: {
                'Content-Type': 'application/json' // Set the content type header
            }
        });

        if (response.data.status == "success") {
            console.log("updated Successfully");
        }
        const updatedUser = response.data; // No need to call response.json()

        // Emit userUpdated event after successful update
        socket.emit('userUpdated', updatedUser);
    } catch (error) {
        console.error('Error updating user:', error);
    }
};


function update() {

    // Get references to our input and button elements
    const userName = document.getElementById("name").value;
    const userEmail = document.getElementById("userEmail").value

    // Get references to the radio buttons
    const radioButtons = document.querySelectorAll('input[name="comment"]');

    // Initialize a variable to store the selected value
    let selectedValue;
    // Loop through each radio button to find the selected one
    radioButtons.forEach(radioButton => {
        if (radioButton.checked) {
            // If the radio button is checked, set the selectedValue to its value
            selectedValue = radioButton.value;
        }
    });

    // Function to get the selected value for task assignment notification
    const getTaskAssignmentNotification = () => {
        const radioButtons = document.querySelectorAll('input[name="yesradio"]');
        let selectedValue;
        radioButtons.forEach(radioButton => {
            if (radioButton.checked) {
                selectedValue = radioButton.nextElementSibling.textContent.trim();
            }
        });
        return selectedValue;
    };

    // Function to get the selected value for task deadline notification
    const getTaskDeadlineNotification = () => {
        const radioButtons = document.querySelectorAll('input[name="duedate"]');
        let selectedValue;
        radioButtons.forEach(radioButton => {
            if (radioButton.checked) {
                selectedValue = radioButton.nextElementSibling.textContent.trim();
            }
        });
        return selectedValue;
    };
    const taskAssignmentNotification = getTaskAssignmentNotification();
    // console.log("Task assignment notification:", taskAssignmentNotification);
    const taskDeadlineNotification = getTaskDeadlineNotification();
    // console.log("Task deadline notification:", taskDeadlineNotification);

    // Construct the data object
    const userData = {
        name: userName,
        email: userEmail,
        // Assuming you have obtained other necessary data as well
        // Add other fields as needed
        partOfGroup: "66120364d03e2b720748426a", // Set this to the appropriate value if available
        // Add notification preferences
        // notifications: {
        //     taskAssignment: taskAssignmentNotification,
        //     taskDeadline: taskDeadlineNotification
        // }
    };

    console.log(userData)
    updateUser(userData, "6611e756242b61e92302aa6c")
}

document.querySelector(".updateProfileForm").addEventListener("submit", (e) => {
    e.preventDefault();
    update();
})
