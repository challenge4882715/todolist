const loginForm = document.getElementById('loginForm');

loginForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    const formData = new FormData(loginForm);
    const userData = Object.fromEntries(formData.entries());
    console.log(userData)

    try {
        const response = await axios.post('http://localhost:6969/justdoit/user/login', userData);
        console.log(response.data);
        loginForm.reset();

        var obj = response.data.data.user
        console.log(obj)
        document.cookie = 'token = ' + JSON.stringify(obj)
        alert('Login successful!');
        console.log(obj)

        window.setTimeout(() => {
            location.assign('/')
        }, 1500)
    } catch (error) {
        console.error('Error logging in:', error.response.data.message);
        alert('Login failed. Please check your credentials and try again.');
    }
});