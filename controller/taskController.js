const Task = require('../models/taskModel');

// Get all tasks
exports.getAllTasks = async (req, res, next) => {
    try {
        const tasks = await Task.find().populate({
            path: 'comments',
            populate: { path: 'Creator' }
        }).populate({
            path: "Creator",
        }).populate({
            path: "taskGroup"
        })

        res.status(200).json({ data: tasks, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};


// Create a new task
exports.createTask = async (req, res, next) => {
    try {
        const task = await Task.create({ ...req.body, taskCreator: req.params.id });
        res.status(201).json({ data: task, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get a single task by ID
exports.getTask = async (req, res, next) => {
    try {
        const task = await Task.findById(req.params.id).populate("Creator comments.Creator").populate("comments");
        if (!task) {
            return res.status(404).json({ message: 'Task not found' });
        }
        res.status(200).json({ data: task, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Update a task by ID
exports.updateTask = async (req, res, next) => {
    try {
        const task = await Task.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!task) {
            return res.status(404).json({ message: 'Task not found' });
        }
        res.status(200).json({ data: task, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Delete a task by ID
exports.deleteTask = async (req, res, next) => {
    try {
        const task = await Task.findByIdAndDelete(req.params.id);
        if (!task) {
            return res.status(404).json({ message: 'Task not found' });
        }
        res.status(200).json({ data: task, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};
