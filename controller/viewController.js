const path = require("path");

exports.getIndexPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "index.html"))
}
exports.getGroupPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "group.html"))
}
exports.getTaskPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "task.html"))
}
exports.getLoginPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "login.html"))
}
exports.getRegistrationPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../", "views", "register.html"))
}