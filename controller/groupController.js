const Group = require("../models/groupModel")

exports.getAllGroups = async (req, res, next) => {
    try {
        const group = await Group.find()
            .populate({
                path: "member",
                populate: { path: "email" }
            })
            .populate({
                path: "tasks",
                populate: { path: "taskTitle" }
            })
            .populate({
                path: "groupCreator",
                populate: { path: "email" }
            });

        res.status(200).json({ data: group, status: "success" });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};


exports.createGroup = async (req, res, next) => {
    try {
        const group = Group.create(req.body);
        res.status(201).json({ data: group, status: "success" })
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

exports.getGroup = async (req, res, next) => {
    try {
        const group = await Group.findById(req.params.id).populate('member');
        // console.log(group.member)
        res.status(200).json({ data: group, status: 'success' });

    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

exports.updateGroup = async (req, res, next) => {
    try {
        const group = await Group.findById(req.params.id).populate("member tasks");
        if (!group) {
            return res.status(404).json({ message: 'Group not found' });
        }
        console.log(req.body.member)
        // Push the new member to the existing member array
        group.member.push(req.body.member);

        // Save the updated group
        const updatedGroup = await group.save();

        res.status(200).json({ data: updatedGroup, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};



exports.deleteGroup = async (req, res, next) => {
    try {
        const group = await Group.findByIdAndDelete(req.params.id);
        if (!group) {
            return res.status(404).json({ message: 'Group not found' });
        }
        res.status(200).json({ data: group, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};
