const Comment = require('../models/commentModel');
const Task = require('../models/taskModel');


// Get all comments
exports.getAllComments = async (req, res, next) => {
    try {
        const comments = await Comment.find().populate("Creator");
        res.status(200).json({ data: comments, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Create a new comment
exports.createComment = async (req, res, next) => {
    try {
        // Create the comment
        const comment = await Comment.create({ ...req.body, Creator: req.params.id });

        // Find the task by ID and push the comment's ID into the comments array
        const task = await Task.findByIdAndUpdate(
            req.body.taskId, // Assuming the taskId is provided in the request body
            { $push: { comments: comment._id } },
            { new: true }
        );

        res.status(201).json({ data: comment, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get a single comment by ID
exports.getComment = async (req, res, next) => {
    try {
        const comment = await Comment.findById(req.params.id).populate("Creator");
        if (!comment) {
            return res.status(404).json({ message: 'Comment not found' });
        }
        res.status(200).json({ data: comment, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Update a comment by ID
exports.updateComment = async (req, res, next) => {
    try {
        const comment = await Comment.findByIdAndUpdate(req.params.id, req.body, { new: true });
        if (!comment) {
            return res.status(404).json({ message: 'Comment not found' });
        }
        res.status(200).json({ data: comment, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Delete a comment by ID
exports.deleteComment = async (req, res, next) => {
    try {
        const comment = await Comment.findByIdAndDelete(req.params.id);
        if (!comment) {
            return res.status(404).json({ message: 'Comment not found' });
        }
        res.status(200).json({ data: comment, status: 'success' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};
