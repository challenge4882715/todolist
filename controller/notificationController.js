const Notification = require('../models/notificationModel');

// Controller function to create a new notification
exports.createNotification = async (req, res) => {
    try {
        const { sender, recipient, message, type } = req.body;

        // Create a new notification
        const notification = new Notification({
            sender,
            recipient,
            message,
            type
        });

        // Save the notification to the database
        await notification.save();

        res.status(201).json({ success: true, data: notification });
    } catch (error) {
        console.error('Error creating notification:', error);
        res.status(500).json({ success: false, error: 'Error creating notification' });
    }
};

exports.getAllNotification = async (req, res) => {
    try {
        const notification = await Notification.find();
        res.json({ success: true, data: notification });
    } catch (error) {

        res.status(500).json({ error: err.message })
    }

}

// Controller function to get all notifications for a user
exports.getNotificationsForUser = async (req, res) => {
    try {
        const { id } = req.params; // Correctly access the id parameter from req.params

        // Find all notifications where the recipient is the specified user
        const notifications = await Notification.find({ recipient: id }) // Use id instead of userId
            .populate('sender', 'username') // Populate the sender field with the username
            .sort({ timestamp: -1 }); // Sort by timestamp in descending order

        res.json({ success: true, data: notifications });
    } catch (error) {
        console.error('Error getting notifications:', error);
        res.status(500).json({ success: false, error: 'Error getting notifications' });
    }
};


// Controller function to delete a notification
exports.deleteNotification = async (req, res) => {
    try {
        const { id } = req.params;

        // Find the notification by ID and delete it
        const notification = await Notification.findByIdAndDelete(id);

        if (!notification) {
            return res.status(404).json({ success: false, error: 'Notification not found' });
        }

        res.json({ success: true, data: notification });
    } catch (error) {
        console.error('Error deleting notification:', error);
        res.status(500).json({ success: false, error: 'Error deleting notification' });
    }
};
